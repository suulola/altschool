

<p>Installing Virtual box is a bit tricky on Mac M1 because of the absence of the Intel Chip which VirtualBox supports</p>

The solution for me was to use Docker and then run a VirtualBox as a container on Docker

Steps
1. Install [Docker Desktop](https://docs.docker.com/desktop/mac/apple-silicon/) 
2. Configure Docker
3. Install VirtualBox on Docker
4. Spin up the Virtual Box

The above process wasn't as smooth as i thought so I had to start my search again for other options

After watching this [YouTube video](https://www.youtube.com/watch?v=uRwnwkdSX-I), I knew I had just 3 options

1. Parrallel - which is the best but will cost me 70 dollars
2. UTM - buggy but free
3. VMWare - So buggy, it's best to stay away from it until its stable.

So I [downloaded UTM](https://mac.getutm.app/) and installed. Another file that will be downloaded is the [Ubuntu 22.04.1 LTS ARM64](https://ubuntu.com/download/server/arm) that will be used.


After launching it from Applications, the first thing you'd see it
![UTM interface](./assets/utm_start.png)

There are two options to Create a Virtual Machine
1. Configure the OS yourself 
2. Work with a pre-configured OS

Unfortunately for Ubuntu, you have to configure it yourself - using the `Create a New Virtual Machine`

An amazingly detailed guide is available on [UTM Website for Ubuntu Installation](https://mac.getutm.app/gallery/ubuntu-20-04)

![UTM Ubuntu guide](./assets/ubuntu_guide.png)

However, the problem with UTM is that, you can't use Vagrant with it. Vagrant is a tool for building and managing virtual machine environments in a single workflow.

To use Vagrant on M1 to create VMs,

1. Install [Docker Desktop](https://docs.docker.com/desktop/mac/apple-silicon/) 
2. Install [Vagrant](https://www.vagrantup.com/downloads)
3. Check to confirm both are install and running

```js
vagrant --version
docker --version
docker info // check docker daemon status
```

5. Initialize a baseboxes from Vagrant. You can check a list of the different [base images here](https://github.com/tknerr/vagrant-docker-baseimages)
```js
vagrant init tknerr/baseimage-ubuntu-20.04 --minimal
```

6. 
```js
vagrant up --provider docker
```

7.

```js
vagrant ssh
```


8. 
```js
sudo apt-get install net-tools
```

9.
```js
ifconfig
```
