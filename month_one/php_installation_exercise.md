### Task: Install PHP 7.4 on your local linux machine using the ppa:ondrej/php package repo.
- Use the add-apt-repository command
- Submit the content of /etc/apt/sources.list and the output of php -v command.

## Solution
1. Update the package index listing
```
sudo apt update
```

2. Install `software-properties-common` to add and remove PPAs

```
sudo apt install software-properties-common -y
```
![software-properties-common](./assets/software_properties.png)


3. Add the apt repository for php to get the latest build packages of PHP 7.4

```
sudo add-apt-repository ppa:ondrej/php
```
![add repository](./assets/add_repo.png)

4. Update the package index listing with the added PPA

```
sudo apt update
```

5. Install the PHP

```
sudo apt install php7.4 -y
```
![install php](./assets/install_php.png)

6. Confirm Installation

```
php -v
```
![confirm php](./assets/confirm_php.png)


7. Output content of `/etc/apt/sources.list`

```
cat /etc/apt/sources.list
```
![sources.list output](./assets/list_output.png)
