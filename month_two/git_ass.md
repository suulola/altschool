# Task: Submit the output of

```bash
git config -l
```

![git config](./assets/git_config.png)

```bash
git remote -v
```
![git remote output](./assets/remote_v.png)

```bash
git log
```
![git log](./assets/log.png)

